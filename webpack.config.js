var webpack = require("webpack");
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: "./src/index.js",
	output: {
		path: __dirname + "/dist/assets",
		filename: "bundle.js",
		publicPath: "assets"
	},
	devServer: {
		inline: true,
		contentBase: './dist',
		port: 3000
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				loader: "babel-loader",
				options: {
					presets: ["latest", "react", "stage-0"]
				}
			},
			// {
			// 	test: /\.json$/,
			// 	exclude: /(node_modules)/,
			// 	loader: "json-loader"
			// },
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					// fallback: "style-loader",
					use: "css-loader"
				}),
				// loader: 'style-loader!css-loader!autoprefixer-loader'
			},
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					// fallback: "style-loader",
					use: ["css-loader", "sass-loader"]
				})
				// loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader'
			}
		]
	},
	plugins: [
		new ExtractTextPlugin("styles.css"),
	]
}







