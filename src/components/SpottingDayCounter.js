import '../stylesheets/ui.scss';
import Wind from "react-icons/lib/ti/weather-windy";
import Rain from "react-icons/lib/ti/weather-shower";
import Calendar from "react-icons/lib/fa/calendar";
import { PropTypes } from "react";

const percentToDecimal = (decimal) => {
    return `${decimal * 100}%`;
}

const calcGoalProgress = (total, goal) => {
    return percentToDecimal(total / goal);
}

export const SpottingDayCount = ({total=70, rainy=20, windy=10, goal=100}) => (
    <div className="spotting-day-count">
        <div className="total-days">
            <span>{total}</span>
                <Calendar />
            <span>days</span>
        </div>
        <div className="rain-days">
            <span>{rainy}</span>
                <Rain />
            <span>days</span>
        </div>
        <div className="wind-days">
            <span>{windy}</span>
                <Wind />
            <span>days</span>
        </div>
        <div>
            <span>
                {calcGoalProgress(
                    total,
                    goal
                )}
            </span>
        </div>
    </div>
);

SpottingDayCount.propTypes = {
  total: PropTypes.number.isRequired,
  rainy: PropTypes.number,
  windy: PropTypes.number,
  goal: PropTypes.number
}
