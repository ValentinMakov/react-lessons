import Wind from "react-icons/lib/ti/weather-windy";
import Rain from "react-icons/lib/ti/weather-shower";
import Calendar from "react-icons/lib/fa/calendar";
import { PropTypes } from "react";

export const SpottingDayRow = ({ airport, date, raining, wind }) => (
    <tr>
        <td>
            {date.getDate()}/
            {date.getMonth() + 1}/
            {date.getFullYear()}
        </td>
        <td>
            {airport}
        </td>
        <td>
            {(raining) ? <Rain /> : null}
        </td>
        <td>
            {(wind) ? <Wind /> : null}
        </td>
    </tr>
);

SpottingDayRow.propTypes = {
    airport: PropTypes.string.isRequired,
    date: PropTypes.instanceOf(Date).isRequired,
    raining: PropTypes.bool,
    wind: PropTypes.bool
}