import { Component } from "react";
import { SpottingDayList } from "./SpottingDayList";
import { SpottingDayCount } from "./SpottingDayCounter";
import { AddDayForm } from "./AddDayForm";
import { Menu } from "./Menu"

export class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            allSpottingDays: [
                {
                    airport: "Sheremetyevo",
                    date: new Date("1/2/2016"),
                    raining: true,
                    wind: false
                },
                {
                    airport: "Domodedovo",
                    date: new Date("2/10/2016"),
                    raining: false,
                    wind: false
                },
                {
                    airport: "Vnukovo",
                    date: new Date("3/30/2016"),
                    raining: true,
                    wind: true
                }
            ]
        }
    }
    countDays(filter) {
        return this.state.allSpottingDays.filter((day) => (filter) ? day[filter] : day).length;
    }
    render() {
        return (
            <div className="app">
                <Menu />
                {
                    (this.props.location.pathname === "/") ?
                        <SpottingDayCount
                            total={this.countDays()}
                            rainy={this.countDays("raining")}
                            windy={this.countDays("wind")} /> :
                        (this.props.location.pathname === "/add-day") ?
                            <AddDayForm /> :
                            <SpottingDayList
                                days={this.state.allSpottingDays}
                                filter={this.props.params.filter} />
                }

            </div>
        )
    }
}