import { createClass, PropTypes } from 'react';
import '../stylesheets/ui.scss';
import Wind from "react-icons/lib/ti/weather-windy";
import Rain from "react-icons/lib/ti/weather-shower";
import Calendar from "react-icons/lib/fa/calendar";

export const SpottingDayCount = createClass({
  propTypes: {
    total: PropTypes.number.isRequired,
    rainy: PropTypes.number,
    windy: PropTypes.number
  },
  getDefaultProps() {
    return {
      // total: 50,
      rainy: 50,
      windy: 15,
      goal: 100
    }
  },
  percentToDecimal(decimal) {
    return `${decimal * 100}%`
  },
  calcGoalProgress(total, goal) {
    return this.percentToDecimal(total / goal)
  },
  render() {
    return (
      <div className="spotting-day-count">
        <div className="total-days">
          <span>{this.props.total}</span>
          <Calendar />
          <span>days</span>
        </div>
        <div className="rain-days">
          <span>{this.props.rainy}</span>
          <Rain />
          <span>days</span>
        </div>
        <div className="wind-days">
          <span>{this.props.windy}</span>
          <Wind />
          <span>days</span>
        </div>
        <div>
          <span>
            {this.calcGoalProgress(
              this.props.total,
              this.props.goal
            )}
          </span>
        </div>
      </div>
    )
  }
});