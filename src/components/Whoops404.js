export const Whoops404 = () =>
    <div>
        <h1>Whoops, the route is not found</h1>
    </div>