import Wind from "react-icons/lib/ti/weather-windy";
import Rain from "react-icons/lib/ti/weather-shower";
import Calendar from "react-icons/lib/fa/calendar";
import { SpottingDayRow } from "./SpottingDayRow";
import { PropTypes } from "react";
import { Link } from "react-router-dom";

export const SpottingDayList = ({ days, filter }) => {
    const filteredDays = (!filter || !filter.match(/rainy|windy/)) ? 
        days:
        days.filter(day => day[filter]);
        
    return (
        <div className="spotting-day-list">
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Airport</th>
                        <th>Raining</th>
                        <th>Wind</th>
                    </tr>
                    <tr>
                        <td colSpan={4}>
                            <Link to="/list-days">
                                All Days
                            </Link>
                            <Link to="/list-days/rainy">
                                Rainy Days
                            </Link>
                            <Link to="/list-days/windy">
                                Windy Days
                            </Link>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    {filteredDays.map((day, i) =>
                        <SpottingDayRow
                            key={i}
                            {...day} />
                    )}
                </tbody>
            </table>
        </div>
    )
}

SpottingDayList.propTypes = {
    days: function (props) {
        if (!Array.isArray(props.days)) {
            return new Error(
                "SpottingDayList should be an array"
            );
        } else if (!props.days.length) {
            return new Error(
                "SpottingDayList must have at least one record"
            );
        } else {
            return null;
        }
    }
}
