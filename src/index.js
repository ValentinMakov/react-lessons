import React from 'react';
import { render } from 'react-dom';
import "./stylesheets/ui.scss"
import { App } from './components/App';
import { HashRouter, Route } from "react-router-dom";
import { Whoops404 } from "./components/Whoops404";

window.React = React;

// render(
// 	<SpottingDayCount rainy={false} />,
// 	document.getElementById('react-container')
// );

render(
	<HashRouter>
		<div>
			<Route path="/" component={App} />
			<Route path="list-days" component={App}>
				<Route path=":filter" component={App}/>
			</Route>
			<Route path="add-day" component={App} />
			<Route path="*" component={Whoops404} />
		</div>
	</HashRouter>,
	document.getElementById('react-container')
);